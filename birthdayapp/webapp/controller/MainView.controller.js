sap.ui.define([
    "be/avelon/birthdayapp/controller/BaseController",
    "sap/ui/core/Fragment",
    "sap/m/MessageToast",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "be/avelon/birthdayapp/model/formatter"
], function (Controller, Fragment, MessageToast, JSONModel, Filter, FilterOperator, formatter) {
    "use strict";

    return Controller.extend("be.avelon.birthdayapp.controller.MainView", {
        formatter: formatter,

        onInit: function () {

            //this.getView().setModel(oModel);
        },

        onPress: function () {
            if (!this.pDialog) {
                this.getView().setModel(new JSONModel(), "addBirthday");
                this.pDialog = this.loadFragment({
                    name: "be.avelon.birthdayapp.view.AddBirthday"
                });
            }
            this.pDialog.then(function (oDialog) {
                oDialog.open();
            });
        },

        onSave: function (oEvent) {
            var oModel = this.getView().getModel();
            var [sDay, sMonth, sYear] = this.getModel("addBirthday").getProperty("/Date").split("/");

            oModel.create("/VerjaardagSet", {
                "Naam": this.getModel("addBirthday").getProperty("/Naam"),
                "Datum": sYear + "-" + sMonth + "-" + sDay + "T00:00",
                "Mail": this.getModel("addBirthday").getProperty("/Mail"),
            }, { success: this.successHandler, error: this.errorHandler });
            oEvent.getSource().getParent().close();
        },

        onPressDelete: function () {
            console.log(this);
            if (!this.pDialog) {
                this.getView().setModel(new JSONModel(), "deleteBirthday");
                this.pDialog = this.loadFragment({
                    name: "be.avelon.birthdayapp.view.DeleteBirthday"
                });
            }
            this.pDialog.then(function (oDialog) {
                oDialog.open();
            });
        },

        // Delete a birthday
        onDelete: function (oEvent) {
            console.log(oEvent);
            var oModel = this.getView().getModel();
            oModel.remove(oEvent.getSource().getBindingContext(), { success: this.successHandler, error: this.errorHandler });
        },

        // Cancel AddBirthday Dialog fragment
        onCancel: function (oEvent) {
            oEvent.getSource().getParent().close();
        },

        successHandler: function () {
            MessageToast.show("Succes!");
        },

        errorHandler: function () {
            console.error();
        },

        // Filter the table with Mail as input
        // Naam not working
        onFilterMails: function (oEvent) {
            var aFilter = [],
            sQuery = oEvent.getParameter("query"),

            oTable = this.getView().byId("bdayTable"),
            oBinding = oTable.getBinding("items");

            if (sQuery) {
                aFilter.push(new Filter("Mail", FilterOperator.Contains, sQuery));
            }
            oBinding.filter(aFilter);          
        },

          // Sort on Mail
          onSortMail: function (oEvent){
            MessageToast.show("Sorting!");

            var oTable = this.getView().byId("bdayTable");
			var oItems = oTable.getBinding("items");
            
			var oSorter = new Sorter("Mail");
			oItems.sort(oSorter);

            MessageToast.show("Sorting complete!");
          },

          // Sort on Naam
          onSortNaam: function (oEvent){
            MessageToast.show("Sorting!");

            var oTable = this.getView().byId("bdayTable");
			var oItems = oTable.getBinding("items");
            
			var oSorter = new Sorter("Naam");
			oItems.sort(oSorter);

            MessageToast.show("Sorting complete!");
          },

          // Sort on Datum
          onSortDatum: function (oEvent){
            MessageToast.show("Sorting!");

            var oTable = this.getView().byId("bdayTable");
			var oItems = oTable.getBinding("items");
            
			var oSorter = new Sorter("Datum");
			oItems.sort(oSorter);

            MessageToast.show("Sorting complete!");
          }
    });
});